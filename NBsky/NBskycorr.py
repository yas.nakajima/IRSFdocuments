#!/usr/bin/env python

import os
import sys
import numpy as np
import glob
from astropy.io import fits
from sklearn import linear_model


def checkfiles():

    flag = 0

    if not os.path.exists('./calib'):
        print ('./calib not found')
        flag = 1

    if not os.path.exists('objectskylist'):
        print ('objectskylist not found')
        flag = 1

    dlist = glob.glob('*.ffiles')
    if len(dlist) == 0:
        print ('yymmdd.ffiles not found')
        flag = 1
    elif len(dlist) > 1:
        print (dlist)
        print ('which yymmdd.ffiles do you want to use?')
        flag = 1

    if flag == 1:
        return '0'
    else:
        return dlist[0]
    

def create_xyf():

    xs = []
    y = []
    for item in sfnum:
        xs.append(dict_mjd[item])
        y.append(dict_delta[item])
    xs = np.array(xs)
    y = np.array(y)

    xt = []
    for item in tfnum:
        xt.append(dict_mjd[item])
    xt = np.array(xt)

    xt = xt - xs[0]
    xs = xs - xs[0]

    xtf = xt.reshape(-1, 1)
    xsf = xs.reshape(-1, 1)
    yf = y.reshape(-1, 1)
    
    return xtf, xsf, yf



def mktsfnum(skyn):
    
    tfnum = []
    sfnum = []
    with open('objectskylist') as fin:
        for line in fin:
            v = line[:-1].split()
            if not v[3] == skyn:
                continue
            
            idx = v[0].split('-')
            for i in range(int(idx[0]), int(idx[1])+1): 
                tfnum.append(str(i).zfill(4))
        
            if ':' in v[2]:                
                group = v[2].split(':')
            
                idx = group[0].split('-')
                for i in range(int(idx[0]), int(idx[1])+1): 
                    sfnum.append(str(i).zfill(4))
                
                idx = group[1].split('-')
                for i in range(int(idx[0]), int(idx[1])+1): 
                    sfnum.append(str(i).zfill(4))
            else:
                idx = v[2].split('-')
                for i in range(int(idx[0]), int(idx[1])+1): 
                    sfnum.append(str(i).zfill(4))
                    
    return tfnum, sfnum



def skysubtract(skyn, yymmdd_dir):

    imsky = fits.getdata('./calib/k'+skyn+'.fits')
    for a, b in zip(tfnum, yc):
        infits = yymmdd_dir + '/kf' + a + '.fits'
        outfits = './NBskycorr/' + infits
        imin, hdr = fits.getdata(infits, header=True)
        factor = b[0] / skydelta[skyn]
        imout = imin - factor * imsky
        fits.writeto(outfits, imout, hdr)



if __name__ == "__main__":

    yymmdd_dir = checkfiles()
    if yymmdd_dir == '0':
        sys.exit()

    os.mkdir('NBskycorr')
    os.mkdir('NBskycorr/' + yymmdd_dir)

    fnum = []
    mjd = []
    delta = []
    for item in sorted(glob.glob(yymmdd_dir + '/*fits')):
        im, hdr = fits.getdata(item, header=True)
        med0 = np.median(im[487:537,487:511])
        med1 = np.median(im[975:1000,975:1000])
        fnum.append(item[-9:-5])
        mjd.append(hdr['MJD'])
        delta.append(med1 - med0)
        
    mjd = np.array(mjd)
    delta = np.array(delta)
    mjdh = (mjd - mjd[0]) * 24

    dict_mjd = {}
    dict_delta = {}
    for i in range(len(fnum)):
        j = fnum[i]
        dict_mjd[j] = mjdh[i]
        dict_delta[j] = delta[i]


    sarr = []
    with open('objectskylist') as fin:
        for line in fin:
            v = line.rstrip().split()
            sarr.append(v[3])

    skyarr = list(set(sarr))

    skydelta = {}
    for skyn in skyarr:
        insky = './calib/k'+skyn+'.fits'
        im, hdr = fits.getdata(insky, header=True)
        med0 = np.median(im[487:537,487:537])
        med1 = np.median(im[975:1000,975:1000])
        skydelta[skyn] = med1 - med0

    print('subtracting ... ')
    for skyn in skyarr:
        tfnum, sfnum = mktsfnum(skyn)
        xtf, xsf, yf = create_xyf()
        lfit = linear_model.LinearRegression()
        # you may like 
        # lfit = linear_model.RANSACRegressor()
        lfit.fit(xsf, yf)
        yc = lfit.predict(xtf)
        skysubtract(skyn, yymmdd_dir)



