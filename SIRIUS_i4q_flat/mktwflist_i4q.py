#!/usr/bin/env python

import os
import sys
import re
import commands
from astropy.io import fits


def readh(date, okey):

    #
    # reading the headers of all the FITS files in ../[date]/rawdata
    # and extracting twiligh flat frames based on the OBJECT name
    # and dividing them into morning and evening based on the LT
    #

    fflag = 0
    eflag = 0
    mflag = 0

    flatname = set([])
    foe = open('eflat.tmp', 'w')
    fom = open('mflat.tmp', 'w')
    flist = os.listdir('../'+date+'/rawdata')
    for file in flist:
        if file.startswith('k') and re.search('fits', file):
            rfile = '../'+date+'/rawdata/'+file
            if file[-2:] == 'ic' or file[-2:] == 'fz':
                hdunum = 1
            else:
                hdunum = 0

            try:
                hdr = fits.getheader(rfile, hdunum)
            except:
                print 'FITS corrupted for %s' % rfile
                continue

            if not 'OBJECT' in hdr or not 'TIME_LT' in hdr:
                continue

            object = hdr['OBJECT']

            if re.search(okey, object):
                if object != 'twflat':
                    flatname.add(object)
                timelthh = int(hdr['TIME_LT'][:2])
                fflag += 1
                if timelthh > 15 and timelthh < 22:
                    print >> foe, file
                    eflag += 1
                if timelthh > 2 and timelthh < 9:
                    print >> fom, file
                    mflag += 1

    foe.close()
    fom.close()

    if len(flatname) != 0:
        for item in flatname:
            print date, item, 'is included' 

    return fflag, eflag, mflag



def bkcheck(mode, date, numarr, lvlarr):

    flag = 0
    fw = open('mktwfwarning.txt', 'aw')
    fo = open('kbackground.txt', 'aw')
    print >> fo, date, numarr[0], lvlarr[0] 
    for i in range(1, len(numarr)):
        print >> fo, date, numarr[i], lvlarr[i], 
        if lvlarr[i] - lvlarr[i-1] < 0 :
            flag += 1
            print >> fo, '*'
        else:
            print >> fo, ''
    print >> fo, ''
    fo.close()
    fw.close()



def pickpairs(mode, date, numarr, lvlarr, skydiff):

    #
    # reversing the order to pick pairs from frames with a higher background level
    #
    numarr.reverse()
    lvlarr.reverse()

    num = 0
    fo = open('kflatlist.0', 'aw')
    for i in range(len(numarr)):
        for j in range(i+1, len(numarr)):
            if lvlarr[i] - lvlarr[j] > skydiff:
                num += 1
                print >> fo, date, numarr[i], numarr[j], lvlarr[i], lvlarr[j], mode
                break
    fo.close()

    return num


def mklist(mode, date, skydiff, maxgood):

    # 
    # fdict = {'frame number(str)':backgroundlevel(float)}
    #
    fdict = {}
    f = open('twflat.tmp')
    for line in f:
        v = line[:-1].split()
        num = v[0][8:12]
        if v[0][0] == 'k':
            fdict[num] = float(v[1])
    f.close()

    if mode == 'e':
        vbool = True
    elif mode == 'm':
        vbool = False

    numarr = [] # frame number
    lvlarr = [] # background level

    #
    # the frames are sorted so that a twilight free frame comes first 
    # saturated frames are excluded 
    #
    for k, v in sorted(fdict.items(), reverse=vbool):
        if v < maxgood:
            numarr.append(k)
            lvlarr.append(v)
        else:
            break
        
    if len(numarr) > 1:               #    2015-09-01   a bug fixed 
        # check if the background level is monotonically increasing or decreasing 
        bkcheck(mode, date, numarr, lvlarr)    

        # pick pairs of frames and write to band+'flatlist'
        num = pickpairs(mode, date, numarr, lvlarr, skydiff)
    else:
        print 'no frame was chosen from k %s %s' % (date, mode)
        print 'background level is too high' 

        num = 0

    return num 


def listofaday(date, okey, skydiff, maxgood):

    tnum = 0

    SCRIPTDIR = os.path.realpath(os.path.dirname(sys.argv[0]))
    cprog = 'twflatlev_i4q'

    #
    # reading the FITS header of the rawdata of the day
    # and counting the number of twilight flat frames 
    # and creating eflat.tmp and mflat.tmp
    #
    fflag, eflag, mflag = readh(date, okey)

    #
    # fflag : the total number of twilight flat frames
    # eflag : the total number of the evening twilight flat frames
    # mflag : the total number of the morning twilight flat frames
    #

    if fflag == 0:
        print 'No flat frames taken on %s' % date
        os.remove('eflat.tmp')
        os.remove('mflat.tmp')
        return tnum

    if eflag > 0:
        os.system('%s/%s  %s eflat.tmp' % (SCRIPTDIR, cprog, date))
        num = mklist('e', date, skydiff, maxgood)
        tnum += num

    if mflag > 0:
        os.system('%s/%s %s mflat.tmp' % (SCRIPTDIR, cprog, date))
        num = mklist('m', date, skydiff, maxgood)
        tnum += num

    os.remove('eflat.tmp')
    os.remove('mflat.tmp')
    os.remove('twflat.tmp')

    return tnum


def exslope():

    SCRIPTDIR = os.path.realpath(os.path.dirname(sys.argv[0]))
    cprog = 'exslopefz_i4q'

    band = 'k'

    snum = 0
    fo = open('kflatlist', 'w')
    f = open('kflatlist.0')
    for line in f:
        v = line[:-1].split()
        fits1 = '../'+v[0]+'/rawdata/'+band+v[0]+'_'+v[1]+'.fits'
        fits2 = '../'+v[0]+'/rawdata/'+band+v[0]+'_'+v[2]+'.fits'
        if os.path.exists(fits1) and os.path.exists(fits2):
            file1 = fits1
            file2 = fits2
        elif os.path.exists(fits1+'.fz') and os.path.exists(fits2+'.fz'):
            file1 = fits1+'.fz[1]'
            file2 = fits2+'.fz[1]'
        elif os.path.exists(fits1+'.ic') and os.path.exists(fits2+'.ic'):
            file1 = fits1+'.ic[1]'
            file2 = fits2+'.ic[1]'
        else:
            print 'Something wrong with compression for %s and %s' % (fits1, fits2)
            print 'ic or fz?'
            continue

        flag = commands.getoutput('%s/%s %s %s' % (SCRIPTDIR, cprog, file1, file2))
        if flag == '1':
            snum += 1
            print >> fo, line[:-1]

    f.close()
    fo.close()

    if snum == 0:
        print 'There is no good flat frame for %s-band.' % band
    else:
        print 'a total of %d pairs are selected for %s' % (snum, band)



def printhelp():

    print 'Usage # mktwflist_q4.py date-list'
    print 'to make the input file for twfcom.py'
    print 'ignoring the 4th quadrant'
    print 'for the K-band only' 
    print 'option'
    print '-noexgap'
    print '-skydiff=float (default 2000)'
    print '-maxgood=float (default 6000)'
    print '-keyword=string (default twflat)'
    
    
if __name__ == "__main__":

    argvs = sys.argv
    argc = len(argvs)

    # default values 
    skydiff = 2000.
    maxgood = 6000.  
    exflag = 0
    okey = 'twflat'
    band = 'k'  # q4 

    # checking the arguments 

    if argc < 2:
        printhelp()
        sys.exit()

    for item in argvs:
        if re.search('help', item):
            printhelp()
            sys.exit()

    for i in range(2, argc):
        if argvs[i] != '-noexgap' and not re.match('-skydiff=', argvs[i])  and not re.match('-maxgood=', argvs[i]) and not re.match('-keyword=', argvs[i]):
            print '%s is an illegal argument' % argvs[i]
            printhelp()
            sys.exit()

    nondefault = ''
    oanum = 1
    for item in argvs:
        if item == '-noexgap':
            exflag = -99
            oanum += 1
            nondefault += ' -noexgap'
        elif item.startswith('-skydiff'):
            try:
                skydiff = float(item[9:])
            except:
                print 'Invalid : '+item
                sys.exit()
            nondefault += ' -skydiff='+str(skydiff)
            oanum += 1
        elif item.startswith('-maxgood'):
            try:
                maxgood = float(item[9:])
            except:
                print 'Invalid : '+item
                sys.exit()
            nondefault += ' -maxgood='+str(maxgood)
            oanum += 1
        elif item.startswith('-keyword'):
            try:
                okey = item[9:]
            except:
                print 'Invalid : '+item
                sys.exit()
            nondefault += ' -keyword='+okey
            oanum += 1

    if oanum == argc:
        print 'Input the list'
        printhelp()
        sys.exit()

    # end of checking the arguments 

    if nondefault != '':
        print nondefault

    # keep the parameters in mktwflist.param
    fo = open('mktwflist.param', 'w')
    if exflag == -99:
        print >> fo, '-noexgap'
    print >> fo, 'skydiff='+str(skydiff)
    print >> fo, 'maxgood='+str(maxgood)
    print >> fo, 'keyword='+okey
    fo.close()

    f = open(argvs[1])
    for line in f:
        date = line[:-1]
        if re.search('\S', date):
            print date
            if not os.path.exists('../'+date+'/rawdata'):
                print 'No rawdata for %s' % date
                continue 
            tnum = listofaday(date, okey, skydiff, maxgood)
    f.close()

    #
    # excluding pairs with the gap at the central column
    #
    if exflag == 0:
        if tnum == 0:
            print 'No frame for k-band flat' 
        else:
            exslope()
    else:
        os.rename('kflatlist.0', 'kflatlist')
        print 'a total of %d pairs are selected for k' % (tnum)

    if os.path.getsize('mktwfwarning.txt') == 0:
        os.remove('mktwfwarning.txt')

    print 'Done'




