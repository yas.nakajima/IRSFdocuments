#!/usr/bin/env python

import os
import sys
import re
from astropy.io import fits


def countpairs(inlist):

    lnum = 0
    f = open(inlist)
    for line in f:
        if not line.startswith('#') and re.search('\S',line[:-1]):
            lnum += 1
    f.close()
    return lnum



def create(myflat):

    SCRIPTDIR = os.path.realpath(os.path.dirname(sys.argv[0]))
    cprog = 'twflatcom_i4q'
    
    lnum = countpairs('kflatlist')
    outfits = 'k' + myflat + '.fits'
    os.system("%s/%s kflatlist %s" % (SCRIPTDIR, cprog, outfits))

    hdu = fits.open(outfits, mode='update')
    phdr = hdu[0].header
    phdr['NCOMBINE'] = lnum
    hdu.close()



if __name__ == "__main__":
    
    argvs = sys.argv
    argc = len(argvs)

    if argc < 2:
        print 'usage: twfcom_i4q.py myflat'
        print 'twfcom_i4q.py creates kmyflat.fits'
        print 'to ignore the 4th quadrant'
        sys.exit()

    flag = 0
    if not os.path.exists('kflatlist'):
        print 'kflatlist not found'
        flag += 1

    if flag > 0:
        sys.exit()

    myflat = argvs[1]

    create(myflat)



