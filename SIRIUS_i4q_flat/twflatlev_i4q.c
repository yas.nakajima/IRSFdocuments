#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "irsf.h"
#include "irsf_cfitsio.h"

/*
twflatlev_i4q.c
gcc -O2 -lm -o twflatlev_i4q twflatlev_i4q.c irsf.c irsf_cfitsio.c -lcfitsio
*/

int main (int argc, char**argv)
{
    FILE    *fl, *fout;
    short   *data;
    short   *oclip;
    int     i, j, xynum=1048576;
    char    line[60], fname1[60], fname2[60];
    char    datedir[10];
    float   midpt, mdev;
    int     fzstat;
    int     ocnum;
    char    **fits1arr, **fits2arr;
    int     ifnum, lnum, jj;
    char    band[2]; 
    char    outfile[15]; 
    int     iter;
    float   lthresh, hthresh, msigma, outvalue;


    if (argc < 3){
      fprintf(stderr, "usage : twflatlev date list\n");
      return 0;
    }
    sprintf(datedir, "%s", argv[1]);

    memcpy(band, argv[2], 1); band[1] = '\0';
    if(strcmp(band, "j") == 0 || strcmp(band, "h") ==0 || strcmp(band, "k") == 0){
      strcpy(outfile, band);
      strcat(outfile, "twflat.tmp");
    } else {
      strcpy(outfile, "twflat.tmp");
    }

    if(NULL==(fl=fopen(argv[2],"r"))){
	printf("\n [%s] can not be opened. \n",argv[2]);
	exit(1);
    }

    lnum=0;
    while(fgets(line, sizeof(line), fl) !=NULL){
      lnum++;
    }
    fclose(fl);

    fits1arr = (char**)malloc(lnum*sizeof(char)*60);
    fits2arr = (char**)malloc(lnum*sizeof(char)*60);
    ifnum = 0;
    fout = fopen(outfile, "w");
    fl = fopen(argv[2], "r");
    while(fgets(line, sizeof(line), fl) != NULL){
      sscanf(line, "%s\n", fname1);

      /*
      strncpy(t, fname1+24, 4);
      t[5] = '\0';
      */

      strcpy(fname2, "../");
      strcat(fname2, datedir);
      strcat(fname2, "/rawdata/");
      strcat(fname2, fname1);

      fits1arr[ifnum] = (char*)malloc(60);
      fits2arr[ifnum] = (char*)malloc(60);
      strcpy(fits1arr[ifnum], fname1);
      strcpy(fits2arr[ifnum], fname2);

      ifnum++;
      strcpy(fname2,"");

    }
    fclose(fl);

    /*
      parameters for getmedianmad_short()
    */
    iter = 3;
    lthresh = 0.;
    hthresh = 20000.;
    msigma = 3.;
    outvalue = -100000.;

    for(jj=0; jj<ifnum; jj++){

      data = (short*)malloc(xynum*sizeof(short));
      fzstat = readfz(data, fits2arr[jj]);

      ocnum = xynum / 4;
      oclip = (short*)malloc(ocnum*sizeof(short));  
      j=0;
      for(i=0; i<xynum; i++){
	if (i % 4 == 0) {
	  /* this part is unique for q4 */ 
	  if (i % 1024 > 511 && i / 1024 < 512){
	    oclip[j] = -10000;
	  } else {
	    oclip[j] = data[i];
	  }
          j++;
	}
      }
      free(data);

      getmedianmad_short(oclip, ocnum, &midpt, &mdev, iter, &lthresh, &hthresh, &msigma, &outvalue);

      fprintf(fout,"%s %.1f\n", fits1arr[jj], midpt);
      free(oclip);
      free(fits1arr[jj]);
      free(fits2arr[jj]);

    }
    fclose(fout);
    free(fits1arr);
    free(fits2arr);

    return 0;

}


