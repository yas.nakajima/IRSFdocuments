#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "irsf.h"
#include "irsf_cfitsio.h"

/*

  exslopefz_i4q.c
  gcc -O2 -o exslopefz_i4q exslopefz_i4q.c irsf.c irsf_cfitsio.c -lcfitsio -lm

*/

float getdev(float mbox[], int mnum, float *med);
int main (int argc, char**argv)
{
    short   *data1, *data2, *data3, *mbox;
    float   *ldata, *sdata;
    int     i, j, k, xynum=1048576;
    char    fname1[80], fname2[80];
    float   midpt, midpt1, midpt2, midpt3, mdev, stdev;
    int     onum;
    int     fzstat;
    int     iter;
    float   lthresh, hthresh, msigma, outvalue;

    if (argc < 3){
      fprintf(stderr, "usage : exslopefz frame1 frame2\n");
      return 0;
    }
    sprintf(fname1, "%s", argv[1]);
    sprintf(fname2, "%s", argv[2]);

    /*
      read the FITS data
    */
    data1 = (short*)malloc(xynum*sizeof(short));
    data2 = (short*)malloc(xynum*sizeof(short));

    fzstat = readfz(data1, fname1);
    if(fzstat==1){
      return 1;
    }

    fzstat = readfz(data2, fname2);
    if(fzstat==1){
      return 1;
    }

    iter = 3;
    lthresh = 0.;
    hthresh = 20000.;
    msigma = 5;
    outvalue = -100000.;

    getmedianmad_short(data1, xynum, &midpt1, &mdev, iter, &lthresh, &hthresh, &msigma, &outvalue);
    getmedianmad_short(data2, xynum, &midpt2, &mdev, iter, &lthresh, &hthresh, &msigma, &outvalue);

    /* printf("%f %f\n", midpt1, midpt2); */

    if(midpt2 > 8000 || midpt1 > 8000){
       free(data1);
       free(data2);
       return 0;
    }

    data3 = (short*)malloc(xynum*sizeof(short));
    if(midpt1 < midpt2){
      for (i=0; i<xynum; i++){
	data3[i] = data2[i] - data1[i];
      }
      midpt3 = midpt2 - midpt1;
    } else {
      for (i=0; i<xynum; i++){
	data3[i] = data1[i] - data2[i];
      }
      midpt3 = midpt1 - midpt2;
    }
    free(data1);
    free(data2);

    iter = 3;
    lthresh = -10000.;
    hthresh = 10000.;
    msigma = 3;
    outvalue = -100000.;
    ldata = (float*)malloc(200*sizeof(float));
    sdata = (float*)malloc(200*sizeof(float));

    for (j=412; j<612; j++){
       mbox = (short*)malloc(512*sizeof(short));
       k=0;
       for (i=xynum/2; i<xynum; i++){
         if(i % 1024 == j){
 	   mbox[k] = data3[i] - data3[i-1];
           k++;
         }
       }
       getmedianmad_short(mbox, 512, &midpt, &mdev, iter, &lthresh, &hthresh, &msigma, &outvalue);
       ldata[j-412] = midpt / midpt3;
       sdata[j-412] = midpt / midpt3;
       free(mbox);
    }
    free(data3);

    getmedianmad(sdata, 200, &midpt, &mdev, iter, &lthresh, &hthresh, &msigma, &outvalue);
    free(sdata);

    stdev = getdev(ldata, 200, &midpt);
    if(fabs(ldata[100]-midpt) <= 3 * stdev || fabs(ldata[100] - midpt) <= 0.010){
      onum = 1;
    } else {
      onum = 0;
    }

    printf("%d\n", onum);

    free(ldata);

    return 0;

}


float getdev(float mbox[], int mnum, float *med)
{
  int    i;
  float  rrr, aaa;

  aaa = *med;
  rrr = 0.0;
  for (i=0; i<mnum; i++){
    rrr = rrr + ((mbox[i]-aaa) * (mbox[i]-aaa));
  }
  rrr = sqrt(rrr/mnum);

  return rrr;
  
}

