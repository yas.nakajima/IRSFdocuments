2018年2月に起こったSIRIUS Kバンドの4th quadrantの問題に対応する
FLAT作成スクリプトについてのメモ。

- Kバンドのフラットをこのスクリプトを用いて作成し、そのフラットを用いてpyIRSFを動かせば問題なく動くはず。

- 作成したもの (i4q stands for 'to ignore the 4th quadrant'. )
* mktwflist_i4q.py
* explopefz_i4q.c
* twfcom_i4q.py
* twflatcom_i4q.c
* twflatlev_i4q.c

これらをiq4.tar.gzにまとめてある。

- iq4.tar.gzのアイコンをクリックしてダウンロードし、展開し、.pyおよび.cの
ファイルをpyIRSFのフォルダに移動する。

- .cファイルをコンパイルする。
* gcc -O2 -o exslopefz_i4q exslopefz_i4q.c irsf.c irsf_cfitsio.c -lcfitsio -lm
* gcc -O2 -o twflatcom_i4q twflatcom_i4q.c irsf.c irsf_cfitsio.c -lcfitsio -lm
* gcc -O2 -lm -o twflatlev_i4q twflatlev_i4q.c irsf.c irsf_cfitsio.c -lcfitsio

- 準備として生データをユーザが**加工しなくても良い**。
* 生データの4th quadrantに-10000とかを入れなくても良いということ
* 一時期、生データを手動で加工して対応してもらってた時期がありました。

- K-bandのみの処理を行う。
* JとHは通常の mktwflist.py dlist -band=j,h twfcom.py -band=j, h で処理する

- 使い方は通常版と同様
* mktwflist_i4q.py dlist (必要に応じてoption)  +
でkflatlistを作成(-band=kは不要)

* twfcom_i4q.py myflat  +
でkmyflat.fitsを作成。(-band=kは不要、kflatlistを読み込む)
4th quadrantには-10000の値が入っている。 +
このようになっていればpyIRSFのパイプラインでは
4th quadrant 全体がbad pixelとして無視して処理される。
